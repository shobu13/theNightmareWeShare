# Generated by Django 2.1.3 on 2018-11-29 19:28

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('nightmare', '0005_auto_20181129_1943'),
    ]

    operations = [
        migrations.AddField(
            model_name='nightmarepart',
            name='number',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='nightmare',
            name='date_creation',
            field=models.DateField(default=datetime.datetime(2018, 11, 29, 19, 28, 0, 734458, tzinfo=utc)),
        ),
    ]
