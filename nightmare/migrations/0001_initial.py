# Generated by Django 2.1.3 on 2018-11-29 18:06

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import markdownx.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Nightmare',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('date_creation', models.DateField(default=datetime.datetime(2018, 11, 29, 18, 6, 4, 446480, tzinfo=utc))),
                ('completed', models.BooleanField(default=False)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='NightmarePart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='nightmare_part/')),
                ('text', markdownx.models.MarkdownxField()),
                ('nightmare', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nightmare.Nightmare')),
            ],
        ),
        migrations.CreateModel(
            name='NightmareSurvey',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('duration', models.DurationField()),
                ('completed', models.BooleanField(default=False)),
                ('part', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nightmare.NightmarePart')),
            ],
        ),
        migrations.CreateModel(
            name='NightmareSurveyProposition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('vote', models.IntegerField(default=0)),
                ('survey', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='nightmare.NightmareSurvey')),
            ],
        ),
        migrations.AddField(
            model_name='nightmarepart',
            name='survey',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='nightmare.NightmareSurvey'),
        ),
    ]
